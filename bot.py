import re
import requests
import telebot

from lxml import html
from config import Config


config = Config()
bot = telebot.TeleBot(config.TOKEN)


def recipes_as_str(recipes_list, header=None):
    header = '' if not header else header
    message_text = header

    # Формируем сообщение с полученными рецептами
    for recipe in recipes_list:
        message_text += '{} /id_{}\n'.format(recipe.get('name'), recipe.get('id'))

    return message_text


def get_pagination_recipes(current_page, data, prev_page, next_page):
    """Формирование клавиатуры для пагинации найденных рецептов"""
    keyboard = telebot.types.InlineKeyboardMarkup()

    # Конвертируем ингредиенты в строку, чтобы передать их в callback.data
    data_to_str = ','.join(data)

    buttons = []

    if prev_page:
        buttons.append(telebot.types.InlineKeyboardButton(
            text='<',
            callback_data='page_{}|{}'.format(current_page - 1, data_to_str)
        ))

    if next_page:
        buttons.append(telebot.types.InlineKeyboardButton(
            text='>',
            callback_data='page_{}|{}'.format(current_page + 1, data_to_str)
        ))

    keyboard.add(*buttons)

    return keyboard


def get_pagination_favorites(current_page, prev_page, next_page):
    """Формирование клавиатуры для пагинации закладок"""
    keyboard = telebot.types.InlineKeyboardMarkup()

    buttons = []

    if prev_page:
        buttons.append(telebot.types.InlineKeyboardButton(
            text='<',
            callback_data='favorite_page_{}'.format(current_page - 1)
        ))

    if next_page:
        buttons.append(telebot.types.InlineKeyboardButton(
            text='>',
            callback_data='favorite_page_{}'.format(current_page + 1)
        ))

    keyboard.add(*buttons)

    return keyboard


def get_recipes_by_keywords(keywords, page=1):
    """Запрос рецептов по ключевым словам, отдаёт словарь с ответом"""

    # Запрос для получения рецептов
    request = requests.get(config.GET_RECIPES, params={'page': page, 'q': ' '.join(keywords)})

    # Ответ в json
    json_response = request.json()

    return json_response


def get_favorite_recipe_inline_button(recipe_id, action=None):
    """Если action == True, значит добавить, если False, удалить из закладок"""
    # Формирует inline-клавиатуру для добавления в закладки
    keyboard = telebot.types.InlineKeyboardMarkup()
    buttons = []

    if not action:
        buttons.append(telebot.types.InlineKeyboardButton(
            'Добавить рецепт в закладки',
            callback_data='add_' + str(recipe_id)
        ))
    else:
        buttons.append(telebot.types.InlineKeyboardButton(
            'Удалить рецепт из закладок',
            callback_data='remove_' + str(recipe_id)
        ))

    keyboard.add(*buttons)

    return keyboard


@bot.message_handler(commands=['start', 'help', ])
def send_welcome(message):
    """Приветствие"""
    bot.send_message(message.chat.id, 'Введите названия ингридиентов через запятую!')


@bot.message_handler(commands=['menu', ])
def send_menu(message):
    """Меню"""
    keyboard = telebot.types.InlineKeyboardMarkup()
    buttons = [telebot.types.InlineKeyboardButton('Закладки', callback_data='favorites'), ]
    keyboard.add(*buttons)

    bot.send_message(message.chat.id, 'Выберите интересующий пункт меню', reply_markup=keyboard)


@bot.message_handler(regexp='^/id_\d+$')
def send_recipe_by_id(message):
    """Отдаёт рецепт по id"""
    recipe_id = re.search(r'^/id_(\d+)$', message.text).group(1)
    recipe = requests.get(config.GET_RECIPE_BY_ID.format(recipe_id),
                          params={'telegram_id': message.from_user.id}).json()

    if not recipe:
        bot.send_message(chat_id=message.chat.id, text='Рецепта не существует')
        return

    # Формируем строку из ингредиентов
    recipe_ingredients = ''
    for recipe_ingredient in recipe.get('ingredients'):
        recipe_ingredients += '{} - _{} {}_\n'.format(
            recipe_ingredient.get('product'), recipe_ingredient.get('quantity'), recipe_ingredient.get('measure')
        )

    # Отправляем название + время приготовления
    bot.send_message(
        chat_id=message.chat.id,
        text='*{}* \nВремя приготовления: _{}_'.format(recipe.get('name'), recipe.get('time')),
        parse_mode='Markdown',
    )

    # Отправляем ингредиенты
    bot.send_message(
        chat_id=message.chat.id,
        text='*Ингредиенты:*\n{}'.format(recipe_ingredients),
        parse_mode='Markdown',
    )

    # Обрезает инструкцию рецепта до 4000 символов
    # splitted_instruction = util.split_string(recipe.get('instruction'), 4000)
    # for text in splitted_instruction:
    #     bot.send_message(chat_id=message.chat.id, text=text)
    splitted_instruction = recipe.get('instruction').split('=|||=')
    for step in splitted_instruction:
        bot.send_message(chat_id=message.chat.id, text=step)

    # Обрезаем и отправляет заметки если они есть
    if recipe.get('notes'):
        notes = html.fromstring(recipe.get('notes'))
        raw_notes = notes.text_content()
        splitted_notes = telebot.util.split_string(raw_notes, 4000)
        for text in splitted_notes:
            bot.send_message(chat_id=message.chat.id, text=text)

    # Желаем приятного аппетита
    bot.send_message(
        chat_id=message.chat.id,
        text='Приятного аппетита!',
        reply_markup=get_favorite_recipe_inline_button(recipe_id, recipe.get('favorite')),
    )


@bot.message_handler(func=lambda message: True)
def send_recipes_by_ingredient(message):
    """Получает строчку ингридиентов через запятую, и посылает сообщение по найденных рецептах"""

    # Приводим ингредиенты к списку
    keywords = [i.strip().lower() for i in message.text.split(',')]

    response = get_recipes_by_keywords(keywords)

    if not response.get('results'):
        message_text = 'Ничего не найдено, уточните поиск'
    else:
        message_text = recipes_as_str(response.get('results'), '<b>По Вашему запросу найдены следующие рецепты:</b>\n')

    bot.send_message(
        chat_id=message.chat.id,
        text=message_text,
        parse_mode='html',
        reply_markup=get_pagination_recipes(
            current_page=1,
            data=keywords,
            prev_page=True if response.get('prev') else False,
            next_page=True if response.get('next') else False,
        )
    )


@bot.callback_query_handler(func=lambda call: call.data.startswith('page_'))
def pagination_recipe_by_ingredient_inline(call):
    """Пагинация по результатам поиска по ингредиентам"""
    # Получаем страницу
    page = int(re.search('^page_(\d+)\|.*$', call.data).group(1))
    # Получаем ингредиенты
    keywords = call.data.split('|', 1)[1].split(',')

    # Новый текст сообщения
    response = get_recipes_by_keywords(keywords, page)

    message_text = recipes_as_str(response.get('results'), 'По Вашему запросу найдены следующие рецепты:\n')

    bot.edit_message_text(
        chat_id=call.message.chat.id,
        message_id=call.message.message_id,
        text=message_text,
        reply_markup=get_pagination_recipes(
            current_page=page,
            data=keywords,
            prev_page=True if response.get('prev') else False,
            next_page=True if response.get('next') else False,
        )
    )


@bot.callback_query_handler(func=lambda call: call.data.startswith('add_'))
def add_recipe_to_favorites(call):
    """Добавляет рецетп в закладки"""
    # Получаем id рецепта
    recipe_id = int(re.search('^add_(\d+)$', call.data).group(1))

    response_user = requests.get(config.GET_USERS, params={'telegram_id': call.from_user.id}).json()
    requests.post(response_user.get('favorites'), data={'user_id': response_user.get('id'), 'recipe_id': recipe_id})

    bot.edit_message_reply_markup(
        chat_id=call.message.chat.id,
        message_id=call.message.message_id,
        reply_markup=get_favorite_recipe_inline_button(recipe_id, True)
    )


@bot.callback_query_handler(func=lambda call: call.data.startswith('remove_'))
def remove_recipe_from_favorites(call):
    """Удаляет рецепт из закладок"""
    # Получаем id рецепта
    recipe_id = int(re.search('^remove_(\d+)$', call.data).group(1))

    response_user = requests.get(config.GET_USERS, params={'telegram_id': call.from_user.id}).json()
    requests.delete(response_user.get('favorites') + str(recipe_id) + '/')

    bot.edit_message_reply_markup(
        chat_id=call.message.chat.id,
        message_id=call.message.message_id,
        reply_markup=get_favorite_recipe_inline_button(recipe_id, False)
    )


@bot.callback_query_handler(func=lambda call: call.data == 'favorites')
def get_favorites(call, page=1):
    """Отправляет рецепты в закладках"""

    # Запрос для получения рецептов
    response_user = requests.get(config.GET_USERS, params={'telegram_id': call.from_user.id}).json()
    request_favorite_recipes = requests.get(response_user.get('favorites'), params={'page': page})

    # Ответ в json
    json_response = request_favorite_recipes.json()

    if json_response.get('results'):
        bot.send_message(
            chat_id=call.message.chat.id,
            text=recipes_as_str(json_response.get('results'), 'Закладки:\n'),
            reply_markup=get_pagination_favorites(
                current_page=page,
                prev_page=True if json_response.get('prev') else False,
                next_page=True if json_response.get('next') else False,
            )
        )


@bot.callback_query_handler(func=lambda call: call.data.startswith('favorite_page_'))
def pagination_favorite_recipes_inline(call):
    page = int(re.search('^favorite_page_(\d+)$', call.data).group(1))
    get_favorites(call, page)


def run_bot():
    bot.polling(none_stop=True, interval=0, timeout=20)
