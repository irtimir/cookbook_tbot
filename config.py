class Config(object):
    TOKEN = ''
    COOKBOOK_URL = 'http://127.0.0.1:5000/cookery'
    GET_RECIPES = COOKBOOK_URL + '/recipes/'
    GET_RECIPE_BY_ID = COOKBOOK_URL + '/recipes/{}/'
    GET_USERS = COOKBOOK_URL + '/users/'
    GET_USER = COOKBOOK_URL + '/users/{}/'
